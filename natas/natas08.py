#!/usr/bin/env python

import requests
import re
import base64
import binascii

username = 'natas8'
password = 'DBfUBfqQG69KvJvJ1iAbMoIpwSNQ9bWe'

url = 'http://%s.natas.labs.overthewire.org/index-source.html' % username

session = requests.Session()
response = session.get(url, auth=(username, password))
content = response.text

# $encodedSecret = "3d3d516343746d4d6d6c315669563362";
# function encodeSecret($secret) {
#    return bin2hex(strrev(base64_encode($secret)));

secret = '3d3d516343746d4d6d6c315669563362'
binary = binascii.unhexlify(secret) # Hexadecimal conversion to binary
reverse = binary[::-1]              # Reverse base64 which is backwards
decode = base64.b64decode(reverse)  # Decode the base64 encoded string

print(secret)
print(binary)
print(reverse)
print(decode)
